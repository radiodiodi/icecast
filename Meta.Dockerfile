FROM alpine
RUN apk add --no-cache bash curl jq
WORKDIR /app

COPY metadata.sh metadata.sh

ENTRYPOINT bash metadata.sh

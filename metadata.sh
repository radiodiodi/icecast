#!/bin/bash

# Hae now_playing -informaatio API:sta ja laita se Icecastin metadataan
# Lokit menee cronista logs/metadata.logiin
#
# Jos haku epaonnistuu, skripti exittaa ennenaikaisesti eikä paivita metadataa

set -e

log() {
	echo "$(date) ${1}"
}

urlencode() {
    local _length="${#1}"
    for (( _offset = 0 ; _offset < _length ; _offset++ )); do
        _print_offset="${1:_offset:1}"
        case "${_print_offset}" in
            [a-zA-Z0-9.~_-]) printf "${_print_offset}" ;;
            ' ') printf + ;;
            *) printf '%%%X' "'${_print_offset}" ;;
        esac
    done
}

URL="${API_URL}/now_playing_ascii"
log "Fetching metadata from ${URL}..."

while true; do
  RESPONSE=`curl ${URL}`

  log "API response:"
  log "${RESPONSE}"

  log "Reading title..."
  TITLE=`echo ${RESPONSE} | jq '.title'`
  if [[ "${TITLE}" == "null" ]]; then
    log "Failed to read title."
    log "Title value: ${TITLE}"
    sleep 5
    continue
  fi

  log "Got title: ${TITLE}"
  ENCODED=$(urlencode "${TITLE}")
  log "Encoded: ${ENCODED}"

  log "Updating AAC stream title..."
  curl -u ${ADMIN_USER}:${ADMIN_PASSWD} "${ICECAST_URL}/admin/metadata?mount=/aac&mode=updinfo&song=${ENCODED}"
  log "Updating MP3 stream title..."
  curl -u ${ADMIN_USER}:${ADMIN_PASSWD} "${ICECAST_URL}/admin/metadata?mount=/mp3&mode=updinfo&song=${ENCODED}"
  log "Updating FLAC stream title..."
  curl -u ${ADMIN_USER}:${ADMIN_PASSWD} "${ICECAST_URL}/admin/metadata?mount=/flac&mode=updinfo&song=${ENCODED}"

  sleep 5

done

set +e

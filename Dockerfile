FROM alpine
RUN apk add --no-cache icecast gettext bash
WORKDIR /app

RUN chmod 777 -R /app

COPY run.sh run.sh
COPY icecast.xml icecast-template.xml

RUN chown nobody -R /usr/share/icecast

ENTRYPOINT sh run.sh

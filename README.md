# Icecast (virta.radiodiodi.fi)

## Icecast in a container (for K8s)

Streaming server, https://icecast.org/

v2.4.3

### ENVs

- HOST
- ADMIN_EMAIL
- SOURCE_PASSWD
- ADMIN_USER
- ADMIN_PASSWD
- RELAY_USER
- RELAY_PASSWD

## Converter

icecast-stream-converter, `ffmpeg` Docker container that converts audio from one format to another. Current setup converts mp3 stream to:

- .ogg
- .opus
- .flac

## icecast-metadata container

Bash script in Docker container that updates metadata for Icecast streams

- API_URL: <https://api.radiodiodi.fi>
- ICECAST_URL: <https://virta.radiodiodi.fi>
